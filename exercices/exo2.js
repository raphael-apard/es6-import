'use strict';

// II. Les fonctions

/**
 * Exercice 2.1
 *
 * Faire une fonction qui retourne true.
 */
export const exo_2_1 = () => {
	console.log('@todo: Exo 2.1');
}


/**
 * Exercice 2.2
 * 
 * Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
 */
export const exo_2_2 = () => {
	console.log('@todo: Exo 2.2');
}