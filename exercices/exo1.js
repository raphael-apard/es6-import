'use strict';

// I. Les boucles

/**
 * Exercice 1.1
 * 
 * Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
 * 
 *  - l'afficher
 *  - incrémenter de 1
 */
export const exo_1_1 = () => {
	console.log('@todo: Exo 1.1');
}


/**
 * Exercice 2.1
 * 
 * Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :
 *
 * - multiplier la première variable avec la deuxième
 * - afficher le résultat
 * - incrémenter la première variable
 */
export const exo_1_2 = () => {
	console.log('@todo: Exo 2.2');
}