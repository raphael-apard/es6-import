'use strict';

/**
 * Imports
 */

 // Soit on importe tout
import * as exo1 from './exercices/exo1.js';

// Soit on choisi ce qu'on importe
import {exo_2_1, exo_2_2} from './exercices/exo2.js';


/**
 * I. Les boucles
 */

// Exercice 1.1
exo1.exo_1_1();

// Exercice 1.2
exo1.exo_1_2();

// ...

/**
 * II. Les fonctions
 */

// Exercice 2.1
exo_2_1();

// Exercice 2.2
exo_2_2();

// ...
